using LinearAlgebra
using JuMP
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
using Convex

include("example_states_light.jl")
include("example_polynomials.jl")



"""takes a cat state |a> + |-a> and computes the robustness to |a><a| + |-a><-a|"""
#=Input:
- a: Complex number to construct cat state |psi_a> = 1/N_{a} (|a> + |-a>)
- lvl: max degree of polynomial p corresponding to witness W_p
- optional  phi: Complex number:  |psi_a,phi> = 1/N (|a> + e^(i phi)|-a>)
- optional: sos_lvl: level in Reznick's hierarchy, by default sos_lvl=0: checks if p is SOS
- optional: specify solver
Output:
- objective value: 1/(1-min_W tr(W*R)) (robustness to |a><a| + |-a><-a|), with witness W, state R
- Witness W_p=#
function detect_cat_states_non_classicality_by_sos(a::Number, lvl::Int64; sos_lvl::Int64=0, phi::Number=0.0, solver=Mosek.Optimizer)

    ###Initialise solver
    solver = optimizer_with_attributes(Mosek.Optimizer, MOI.Silent() => true)
	model= Model(solver)

    ###Initalise polynomial
    # polynomial variables 
    @polyvar x y
    #decision variables 
    @variables(model, begin
        W[i=0:lvl, j=0:lvl; i+j<=lvl]
    end) 

    p = sum([W[i,j] * x^i * y^j for (i,j) in eachindex(W)])

    #For each entry in W compute and assign
    #positive moment expectation values for objective function
    R = Dict{Tuple, Number}()
    #Normalisation expectation values for normalisation constraint
    T = Dict{Tuple, Number}()
    for i in eachindex(W)
        R[i] = pos_mom_cat_state_expectation_value(a, phi, i)
        T[i] = normalisation_expectation_value(i, a, phi)
    end 

    #objective function
    t = sum([W[i]*R[i] for i in eachindex(W)])

    #SOS constraint
    @constraint(model, (1+x^2+y^2)^(sos_lvl) * p in SOSCone())

    # normalisation constraints
    u = real(sum([T[i]*W[i] for i in eachindex(W)]))

    @constraint(model, u == 1.0)

    @objective(model, Min, t)

    ###Solve with SDP solver
    optimize!(model)

    @show termination_status(model)

    x = objective_value(model)
    
    return 1/(1-x), value.(W)
end 



"""takes a state rho = sum_ij rho_ij |i><j| and computes the robustness to int da <a|rho|a> |a><a|"""
#=Input:
- rho: Fock state rho = sum_ij rho_ij |i><j|
- lvl: max degree of polynomial p corresponding to witness W_p
- optional: sos_lvl: level in Reznick's hierarchy, by default sos_lvl=0: checks if p is SOS
- optional: specify solver
Output:
- objective value: 1/(1-min_W tr(W*R)) (robustness to |a><a| + |-a><-a|), with witness W, state R
- Witness W_p=#
function detect_fock_state_non_classicality_by_sos(rho::Matrix, lvl::Int64; sos_lvl::Int64=0, solver=Mosek.Optimizer)

    ###Initialise solver
    solver = optimizer_with_attributes(solver, MOI.Silent() => true)
	model= Model(solver)

    ###Initialise polynomial
    #polynomial variables 
    @polyvar x y
    #decision variables 
    @variables(model, begin
        W[i=0:lvl, j=0:lvl; i+j<=lvl]
    end) 

    p = sum([W[i,j] * x^i * y^j for (i,j) in eachindex(W)])

    #For each entry in W compute and assign
    #positive moment expectation values for objective function
    R = Dict{Tuple, Number}()
    #Normalisation expectation values for normalisation constraint
    T = Dict{Tuple, Number}()
    for i in eachindex(W)
        R[i] = pos_mom_fock_expectation_value(i, rho)
        T[i] = fock_normalisation_expectation_value(i, rho)
    end 

    ###Initialise SDP
    #objective function
    t = sum([W[i]*R[i] for i in eachindex(W)])

    #SOS constraint
    @constraint(model, (1+x^2+y^2)^(sos_lvl) * p in SOSCone())

    #normalisation constraints
    u = real(sum([T[i]*W[i] for i in eachindex(W)]))

    @constraint(model, u == 1.0)

    @objective(model, Min, t)

    ###Solve SDP
    optimize!(model)

    @show termination_status(model)

    x = objective_value(model)

    print("The objective value is: tr(W*rho)=", x, ".\n")

    return 1/(1-x), value.(W)
end 


"""Computes moment matrix of Fock states"""
#=Input:
- rho: state expanded in Fock basis
- lvl: to compute moment matrix degree 2D=lvl
Output:
- Moment matrix =#
function fock_moment_matrix(rho::Matrix, lvl::Int64)

    d = Int(lvl/2)
    exp = [[0, 0]]

    for k in 1:d
        exp = vcat(exp, sort([[i,j] for i in 0:d for j in 0:d if i+j == k], rev=true))
    end 

    N = length(exp)
    mat = zeros(Float64, N, N)

    for i in 1:N
        for j in 1:N
            sum = exp[i] + exp[j]
            x = (sum[1], sum[2])
            mat[i, j] = pos_mom_fock_expectation_value(x, rho)
        end 
    end 

    return mat
end 

function fock_moment_matrix(rho, lvl::Int64)

    d = Int(lvl/2)
    exp = [[0, 0]]

    for k in 1:d
        exp = vcat(exp, sort([[i,j] for i in 0:d for j in 0:d if i+j == k], rev=true))
    end 

    N = length(exp)
    mat = Matrix{Any}(undef, N, N)

    for i in 1:N
        for j in 1:N
            sum = exp[i] + exp[j]
            x = (sum[1], sum[2])
            mat[i, j] = pos_mom_fock_expectation_value(x, rho)
        end 
    end 
    
    return real(mat)
end 


"""Computes nonclassical state rho that minimises tr(W_P*rho) for a given nonnegative polynomial P"""
#=input:
- P: a nonnegative polynomial saved as a dictionary: map the exponents to its coefficient (as constructed in example_polynomials.jl)
- cut_off:  maximal number of photons in support of the state
- optional: semidefinite programming solver
- optional: pos_mom: additional constraint on the state to be not detectable by SOS polynomials of degree 
output:
- objective value: min_rho tr(W_P*rho),  witness W_P corresponding to polynomial P, state rho
- termination status of solver
- minimizer state rho=#
function maximal_non_classical_state_given_polynomial(P::Dict, cut_off::Int64; solver=Mosek.Optimizer, pos_mom=nothing)

    ###Initialise SDP solver
    solver = optimizer_with_attributes(solver, MOI.Silent() => true)
	model= Model(solver)

    ###Initialise state R
    N = cut_off + 1
    @variable(model, R[1:N, 1:N] in HermitianPSDCone())

    ###Initialise SDP

    #normalisation constraint
    @constraint(model, tr(R) == 1.0)

    #additional positive moment constraint
    #so that R is not detectable by SOS polynomials of this order
    if !isnothing(pos_mom)
        #moment matrices up to degree pos_mom
        G = fock_moment_matrix(R, pos_mom)
        f = size(G)[1]
        #Constraint to be PSD
        @variable(model, S[1:f, 1:f] in PSDCone())
        @constraint(model, S.== G)
    end 

    #objective function
    s = sum([P[i]*pos_mom_fock_expectation_value(i, R) for i in keys(P)])
    @objective(model, Min, s)

    ###Solve SDP
    optimize!(model)

    #@show termination_status(model)

    return objective_value(model), termination_status(model), value.(R)
end 


"""Helper functions"""

#computes the inner product <beta|alpha> 
function coherent_inner_product(alpha::Number, beta::Number)
    x = exp(-1/2*(abs(alpha)^2 + abs(beta)^2) + alpha*adjoint(beta))
    return x 
end 


#computes the inner product <beta|:x^m p^n:|alpha> for coherent states alpha and beta
function coherent_matrix_element(alpha::Number, beta::Number, m::Int64, n::Int64)
    a = ((alpha + adjoint(beta))/(2.0))^m 
    b = ((alpha - adjoint(beta))/(2.0 * im))^n
    x = coherent_inner_product(beta, alpha) * a * b
    return x
end


#computes the integral of  x^n * exp(-x^2 + ax)
function gauss_integral(n::Int64, a::Number)
    N = exp((a^2)/4)
    c = 0.0
    for i in 0:n 
        if i % 2 == 0
            c += binomial(n, i) * (a/2)^(n-i) * sqrt(pi) * ((doublefactorial(i-1))/(2^(i/2)))
        end 
    end 
    return N*c 
end 

#computes double factorial
function doublefactorial(number)
    fact = one(number)
    for m in iseven(number)+1:2:number
        fact *= m
    end
    return fact
end

#computes <psi_a|:x^m p^n:|psi_a> with |psi_a> = 1/N (|a> + e^(i phi)|-a>)
#called from detect_cat_states_non_classicality_by_sos(...)
function pos_mom_cat_state_expectation_value(a::Number, phi::Number, tp::Tuple)
    N = 2*(1+exp(-2*abs(a)^2)*cos(phi))
    x = (1/N) * real(coherent_matrix_element(a, a, tp[1], tp[2]) + coherent_matrix_element(-a, -a, tp[1],tp[2]) + 2*real(exp(im*phi)*coherent_matrix_element(-a, a,tp[1],tp[2])))
    return x 
end 


#computes the expectation value of :x^l p^m: in the noisy state
#classical state constructed from |a> + |-a>
#called from detect_cat_states_non_classicality_by_sos(...)
function normalisation_expectation_value(x::Tuple, a::Number, phi::Number)
    norm = 2 * (1 + exp(-2 * abs(a)^2)*cos(phi))
    l = x[1]
    m = x[2]

    N = (2*exp(-abs(a)^2))/(norm * pi) 

    c = 0.0 

    c += exp(im*phi)*gauss_integral(l, -2*im*imag(a)) * gauss_integral(m, 2*im*real(a))

    c += exp(-im*phi)*gauss_integral(l, 2*im*imag(a)) * gauss_integral(m, -2*im*real(a))
    
    c += gauss_integral(l, 2*real(a)) * gauss_integral(m, 2*imag(a))

    c += gauss_integral(l, -2*real(a)) * gauss_integral(m, -2*imag(a))

    return N * c 
end 

#compute entries of moment matrices of fock states
#called from detect_fock_states_non_classicality_by_sos(...)
#called from fock_moment_matrix(...)
function pos_mom_fock_expectation_value(x::Tuple, rho)
    l = x[1]
    m = x[2]

    N = size(rho)[1]

    c = 0.0

    for i in 0:N-1
        for j in 0:N-1
            for r in 0:l
                for s in 0:m
                    if j-s-r == i-m-l+s+r && j-s-r >= 0
                        val = (1/(2^(l+m) * im^m)) * rho[i+1, j+1]

                        val *= binomial(l,r) * binomial(m,s) * (-1)^s


                        p = prod([sqrt(j-a) for a in 0:s+r-1])
                        val *= p

                        p = prod([sqrt(i-b) for b in 0:m+l-s-r-1])
                        val *= p

                        c += val 
                    end 
                end
            end 
        end 
    end 

    return real(c)
end

#computes normalisation expectation value for fock states
#called from detect_fock_states_nonclassicality_by_sos(...)
function fock_normalisation_expectation_value(x::Tuple, rho::Matrix)
    l = x[1]
    m = x[2]

    N = size(rho)[1]

    c = 0.0

    for i in 0:N-1
        for j in 0:N-1
            for r in 0:i
                for s in 0:j
                    val = rho[i+1, j+1]/pi

                    val *= 1/(sqrt(factorial(i))*sqrt(factorial(j)))
                    val *= (-im)^r * im^s
                    val *= binomial(i, r) * binomial(j, s)

                    exp_1 = l+i+j-s-r
                    exp_2 = m+s+r

                    val *= gauss_integral(exp_1,0) * gauss_integral(exp_2,0)

                    c += val 
                end
            end 
        end 
    end


    return real(c)
end 

