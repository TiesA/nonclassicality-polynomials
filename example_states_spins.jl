using LinearAlgebra
using Convex 
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares

"""Example states"""

#Dicke states 
function Dicke(i::Int64,k::Int64)
    M = zeros(ComplexF64, k, k)
    M[i, i] = 1.0 
    return M
end 

#GHZ like states
function generalized_ghz_state(k::Int64)
    M = zeros(ComplexF64, k+1, k+1)
    M[1, 1] = M[k+1, k+1] = M[1, k+1] = M[k+1, 1] = 1.0
    M /= 2
    return M 
end 

#spin-coherent states 
function spin_coherent_state_vector(k::Int64, z::Number)
    v = zeros(ComplexF64, k+1)

    for i in 0:k
        v[i+1] = sqrt(binomial(k,i)) * z^i
    end 

    v /= norm(v) 

    return v
end 

#spin cat state 
function spin_cat_state(k::Int64, z::Number)
    v_1 = spin_coherent_state_vector(k,z)
    v_2 = spin_coherent_state_vector(k,-z)

    v = v_1 + v_2

    v /= norm(v)

    return v*adjoint(v)
end 


#random states 

#Random state (just uniform, no ensemble), new uniformly distributed version
function Random_symmetric_qubit_state(d::Int64)
	pure_state = RandomPureState(d^2)
	M = partialtrace(pure_state, 2, [d, d])
	return M
end

#Random pure state
function RandomPureState(d::Int64)
	M=randn(Complex{Float64}, d)
	M= M*adjoint(M)
	M= M/tr(M)
	return M 
end

#separable symmetric states given by Vandermonde vectors 
function Separable_symmetric_qubit_state(d::Int64, z::Number)
    u = [(z^i)*sqrt(binomial(d,i)) for i in 0:d]
    u = normalize(u)
    mat = u*adjoint(u)
end 

#bound entangled states

function bound_entangled_A()
    mat = zeros(ComplexF64, 5, 5)
    mat[1, 1] = 0.183
    mat[2, 2] = 0.254
    mat[3, 3] = 0.167
    mat[4, 4] = 0.176
    mat[5, 5] = 0.22
    mat[2, 5] = mat[5, 2] = - 0.059
    return mat
end 

function bound_entangled_B()
    mat = zeros(ComplexF64, 6, 6)
    mat[1, 1] = 0.174
    mat[2, 2] = 0.147
    mat[3, 3] = 0.182
    mat[4, 4] = 0.153
    mat[5, 5] = 0.174
    mat[6, 6] = 0.17
    mat[2, 6] = mat[6, 2] = - 0.0137
    return mat
end

#by Tura et al., 2018
function bound_entangled_C()
    mat = zeros(ComplexF64, 5, 5)
    mat[1, 1] = 7 * sqrt(7)
    mat[2, 2] = 12 * sqrt(7)
    mat[3, 3] = 12 * sqrt(7)
    mat[4, 4] = 12 * sqrt(7)
    mat[5, 5] = 7 * sqrt(7)
    mat[2, 5] = mat[5, 2] = -  2 * sqrt(15)
    mat /= 50 * sqrt(7) 
    return mat
end 

#by Tura 
function bound_entangled_D()
    mat = zeros(ComplexF64, 6, 6)
    mat[1, 1] = mat[6, 6] =  5.0
    mat[2, 2] = mat[3, 3] = mat[4, 4] = mat[5, 5] = 10.0
    mat[1, 6] = mat[6, 1] = 1.0
    mat /= 50.0
    return mat
end 

#Z \in (0,\infty) and o \in {-1, 1}, k an odd number >= 5
function bound_entangled_family(k::Int64, Z::Float64=1.0, o::Float64=1.0)
    mat = zeros(ComplexF64, k+1, k+1)
    mat[1, k+1] = mat[k+1, 1] = o
    K = Int64((k-1)/2)
    lambda = zeros(ComplexF64, K+1)
    lambda[1] = 1.0
    lambda[2] = 1.0 + Z 
    for i in 3:K+1
        lambda[i] = (2 + Z)*lambda[i-1] - lambda[i-2]
    end 

    for i in 1:K+1
        mat[i, i] = binomial(k, i-1)*lambda[K+2-i]
        mat[k+2-i, k+2-i] = mat[i, i]
    end

    mat /= 2*(4+Z)^K
    return mat
end 