using LinearAlgebra
using JuMP
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
using Convex

"""Example states for nonclassicality of light"""

#Random states

function RandomPureState(d::Int64)
	M=randn(Complex{Float64}, d)
	M= M*adjoint(M);
	M= M/tr(M);
	return M
end

function RandomState(d::Int64)
    M = RandomPureState(d^2)
    L = partialtrace(M, 2, [d, d])
    return L 
end 

# Fock states 

function fock_state(N::Int64)
    mat = zeros(ComplexF64, N+1, N+1)
    mat[N+1, N+1] = 1.0
    return mat
end 

function test_state_A()
    v = [sqrt(3)/2, 1/2]
    return v*adjoint(v)
end 

#See appendix B.5
#This state R (rounded) solves min_R tr(W_p*R) with witness W_p corresponding to the Motzkin polynomial
#photon cutoff = 10, nonclassicality cannot be detected by degree 6 SOS polynomial
function test_state_motzkin(p::Float64=0.06619)
    p_1 = p
    p_2 = 1 - p_1 

    v_1 = zeros(ComplexF64, 11)
    v_1[2] = 0.69896
    v_1[6] = -0.68135
    v_1[10] = -sqrt(1 - v_1[2]^2 - v_1[6]^2)

    v_2 = zeros(ComplexF64, 11)
    v_2[3] = -0.94138
    v_2[7] = 0.3124
    v_2[11] = sqrt(1 - v_2[3]^2 - v_2[7]^2)

    R = p_1 * v_1 * adjoint(v_1) + p_2 *v_2 * adjoint(v_2)

    return R 
end 