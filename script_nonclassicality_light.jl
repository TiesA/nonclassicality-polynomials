using LinearAlgebra
using Convex 
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
using CSV
using DataFrames

include("nonclassicality_light.jl")
include("example_states_light.jl")
include("example_polynomials.jl")

#print("\n\n")
#print("This script refers to Figure 2 in the paper, as well as Appendix B.5.\n\n")
#sleep(2)
print("We construct nonclassical states that cannot be detected by sum of squares (SOS) polynomials of degrees 6, 8 and 10 respectively, but are detected by examples of nonnegative polynomials that are not SOS:\n\n")
sleep(2)

#Construct Motzkin Polynomial
print("We first load the Motzkin Polynomial M(x,y)= x^4*y^2 + x^2*y^4 - 3*x^2*y^2 + 1 .\n")
print("The Motzkin Polynomial is nonnegative but it is not a sum of squares.\n\n")
#Saves Motzkin Polynomial as a dictionary
M = motzkin_poly_coeffs()

sleep(2)
print("We search for a nonclassical state R with a maximal number n_max of photons that minimises tr(W_M*R) for W_M being the observable corresponding to the Motzkin polynomial.\n")
sleep(2)
#Compute min_R tr(W_M*R) with photon number cutoff n_max = 10, given the Motzkin Polynomial M as witness
opt, status, R = maximal_non_classical_state_given_polynomial(M, 10)
print("With a photon number cutoff of n_max=10, the Motzkin polynomial detects the nonclassical state R as a minimizer so that tr(W_M*R)=", opt, "<0, which certifies the nonclassicality of R.\n\n")
sleep(2)
print("We now additionally contrain R to be not detectable by SOS polynomials of degree up to D.\n\n")
sleep(2)
print("As an example, we can detect the state R given in Appendix B.5 with n_max = 10 with the Motzkin Polynomial (which is of degree 6) but not with a SOS polynomial of up to degree 6.\n")
#Compute min_R tr(W_M*R) with photon number cutoff n_max = 10, given the Motzkin Polynomial M as witness
#Constrain to be not detectable by SOS polynomials of degree 6
opt, status, R = maximal_non_classical_state_given_polynomial(M, 10, pos_mom = 6)
print("For that state R, we have tr(W_M*R)=", opt, "<0, which certifies the nonclassicality of R.\n\n")
sleep(2)
print("Now reproduce the results in Figure 2 of the paper.\n")
print("We iterate over the photon cutoff n_max.\n\n")

#Specify photon cutoff
max_cutoff = 40
#Constrain to be not dectectable by degree 6, resp. 8, resp. 10 SOS polynomials 
SOS_degrees = [6,8,10]
#for saving
opt_values = [zeros(Float64, max_cutoff) for i in 1:length(SOS_degrees)]
cutoffs = range(1,max_cutoff)

for d=1:length(SOS_degrees)
    print("Find nonclassical state that is detectable by the Motzkin Polynomial but not by degree ", SOS_degrees[d], " SOS polynomial:\n\n")
    sleep(2)
    for cutoff=1:max_cutoff
        local opt
        local status
        local R
        #Compute min_R tr(W_M*R) with variable photon number cutoff, given the Motzkin Polynomial M as witness
        #Constrain to be not detectable by SOS polynomials of degrees in array SOS_degrees
        opt, status, R = maximal_non_classical_state_given_polynomial(M, cutoff, pos_mom = SOS_degrees[d])
        print("For photon cutoff n_max=", cutoff," we find the minimising state R such that tr(W_M*R)=", opt,".\n")
        opt_values[d][cutoff] = opt
        if opt < 0
            print("We detect nonclassicality with the Motzkin polynomial but not with a SOS polynomial of degree ", SOS_degrees[d], ".\n\n")
        else 
            print("We do not detect nonclassicality with the Motzkin polynomial since tr(W_M*R)>0.\n\n")
        end
    end
    print("\n\n")
    sleep(2)
end

#Write Data in csv.file 
df = DataFrame(photon_cutoff = cutoffs, opt_value_degree_6 = opt_values[1], opt_value_degree_8 = opt_values[2], opt_value_degree_10 = opt_values[3])
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Nonclassicality_of_light_detection_by_Motzkin_polynomial.csv")
CSV.write(csv_file_path, df)

print("The results are saved to the file Nonclassicality_of_light_detection_by_Motzkin_polynomial.csv in your current directory and resemble the results in Figure 2 of the paper.\n\n")
sleep(2)
print("We proceed with producing the data for Figure 4 in Appendix B.5 analougously.\n\n")
print("\n\n")
sleep(2)




###Robinson Polynomial
print("The Robinson polynomial Ro(x,y)= x^6 - x^4*y^2 - x^4 - x^2*y^4 + 3*x^2*y^2 -x^2 + y^6 - y^4 - y^2 + 1 is another example for a nonnegative polynomial that is not SOS.\n\n")
Ro = robinson_poly_coeffs()
sleep(2)
print("We search for a nonclassical state R with a maximal number n_max of photons that minimises tr(W_{Ro}*R) for W_{Ro} being the observable corresponding to the Robinson polynomial.\n\n")
print("We iterate over the photon cutoff n_max and additionally constrain R to be not detectable by SOS polynomials of degree 6, 8 and 10 respectively.\n\n")

#Specify photon cutoff
max_cutoff = 40
#Constrain to be not dectectable by degree 6, resp. 8, resp. 10 SOS polynomials 
SOS_degrees = [6,8,10]
#for saving
opt_values = [zeros(Float64, max_cutoff) for i in 1:length(SOS_degrees)]
cutoffs = range(1,max_cutoff)

for d=1:length(SOS_degrees)
    #print("Find nonclassical state that is detectable by the Robinson Polynomial but not by degree ", SOS_degrees[d], " SOS polynomial:\n\n")
    #sleep(2)
    for cutoff=1:max_cutoff
        local opt
        local status
        local R
        #Compute min_R tr(W_M*R) with variable photon number cutoff, given the Robinson Polynomial M as witness
        #Constrain to be not detectable by SOS polynomials of degrees in array SOS_degrees
        opt, status, R = maximal_non_classical_state_given_polynomial(Ro, cutoff, pos_mom = SOS_degrees[d])
        #print("For photon cutoff n_max=", cutoff," we find the minimising state R such that tr(W_{Ro}*R)=", opt,".\n")
        opt_values[d][cutoff] = opt
        #if opt < 0
        #    print("We detect nonclassicality with the Robinson polynomial but not with a SOS polynomial of degree ", SOS_degrees[d], ".\n\n")
        #else 
        #    print("We do not detect nonclassicality with the Robinson polynomial since tr(W_{Ro}*R)>0.\n\n")
        #end
    end
    #print("\n\n")
    #sleep(2)
end

#Write Data in csv.file 
df = DataFrame(photon_cutoff = cutoffs, opt_value_degree_6 = opt_values[1], opt_value_degree_8 = opt_values[2], opt_value_degree_10 = opt_values[3])
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Nonclassicality_of_light_detection_by_Robinson_polynomial.csv")
CSV.write(csv_file_path, df)

print("The results are saved to the file Nonclassicality_of_light_detection_by_Robinson_polynomial.csv in your current directory and resemble the results in Figure 4 in Appendix B.5 .\n\n")
sleep(2)
print("\n\n")




###Choi-Lam polynomial
print("The Choi-Lam polynomial CL(x,y)= x^4*y^2 -3x^2*y^2 + y^4 + x^2 is another example for a nonnegative polynomial that is not SOS.\n\n")
CL = choi_lam_poly_coeffs()
sleep(2)
print("We search for a nonclassical state R with a maximal number n_max of photons that minimises tr(W_{CL}*R) for W_{CL} being the observable corresponding to the Choi-Lam polynomial.\n\n")
print("We iterate over the photon cutoff n_max and additionally constrain R to be not detectable by SOS polynomials of degree 6, 8 and 10 respectively.\n\n")
#Specify photon cutoff
max_cutoff = 40
#Constrain to be not dectectable by degree 6, resp. 8, resp. 10 SOS polynomials 
SOS_degrees = [6,8,10]
#for saving
opt_values = [zeros(Float64, max_cutoff) for i in 1:length(SOS_degrees)]
cutoffs = range(1,max_cutoff)

for d=1:length(SOS_degrees)
    #print("Find nonclassical state that is detectable by the Choi-Lam Polynomial but not by degree ", SOS_degrees[d], " SOS polynomial:\n\n")
    #sleep(2)
    for cutoff=1:max_cutoff
        local opt
        local status
        local R
        #Compute min_R tr(W_{CL}*R) with variable photon number cutoff, given the Choi-Lam Polynomial CL as witness
        #Constrain to be not detectable by SOS polynomials of degrees in array SOS_degrees
        opt, status, R = maximal_non_classical_state_given_polynomial(CL, cutoff, pos_mom = SOS_degrees[d])
        #print("For photon cutoff n_max=", cutoff," we find the minimising state R such that tr(W_{CL}*R)=", opt,".\n")
        opt_values[d][cutoff] = opt
        #if opt < 0
        #    print("We detect nonclassicality with the Choi-Lam polynomial but not with a SOS polynomial of degree ", SOS_degrees[d], ".\n\n")
        #else 
        #    print("We do not detect nonclassicality with the Choi-Lam polynomial since tr(W_{Choi-Lam}*R)>0.\n\n")
        #end
    end
    #print("\n\n")
    #sleep(2)
end

#Write Data in csv.file 
df = DataFrame(photon_cutoff = cutoffs, opt_value_degree_6 = opt_values[1], opt_value_degree_8 = opt_values[2], opt_value_degree_10 = opt_values[3])
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Nonclassicality_of_light_detection_by_Choi-Lam_polynomial.csv")
CSV.write(csv_file_path, df)

print("The results are saved to the file Nonclassicality_of_light_detection_by_Choi-Lam_polynomial.csv in your current directory and resemble the results in Figure 4 in Appendix B.5 .\n\n")