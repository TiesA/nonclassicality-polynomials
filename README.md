# Nonclassicality detection with nonnegative polynomials

This code supplements the paper ["Revealing hidden physical nonclassicality with nonnegative polynomials"](https://arxiv.org/abs/2403.09807) by T.-A. Ohst, B. Yadin, B. Ostermann, T. de Wolff, O. Gühne and H.-C. Nguyen.

We developed methods based on **polynomial nonnegativity** to investigate **nonclassicality** of **light** (single mode harmonic oscillator) and of **spin-$\frac{1}{2}$ systems** systems (symmetric subspace of multi-qubit system).

### Nonclassicality of light

We aim to solve the following optimisation problem in order to detect the nonclassicality of a state $\rho$ of the harmonic oscillator:
$$ v_{D} = \min_W  \text{tr}(W\rho) $$
$$ \text{s.t. } p_W(\bar{\alpha}, \alpha) := \bra{\alpha} W \ket{\alpha}\geq 0, \text{deg}(p_W)\leq D, \text{tr}(W\rho_{0}) = 1$$
 
This optimisation is performed over observables $W$ that are hermitian operators of the form $W = \sum_{ij} w_{ij} a^{\dagger i} a^{j}$ and can serve as a **nonclassicality witness** if the real valued polynomial $p_W(\bar{\alpha}, \alpha) = \sum_{ij} w_{ij} \bar{\alpha}^{i} \alpha^{j}$ in $\alpha$ and its complex conjugate $\bar{\alpha}$ is nonnegative. The maximal polynomial degree $D$ is used to solve the problem for the respective available moment data $\langle a^{\dagger i} a^{j} \rangle_{\rho}$, i.e. $i+j \lt D$. The state $\rho_0$ is some fixed classical state for which a typical choice is $$\rho_{0} = \int_{\mathbb{R}^2} d \alpha \bra{\alpha} \rho \ket{\alpha} \ket{\alpha} \bra{\alpha}.$$

Given a nonclassicality witness $W$, we detect the nonclassicality of $\rho$ if $v_{D} \lt 0$.

### Nonclassicality of spin-$\frac{1}{2}$ systems
Given a state $\rho$ supported on the symmetric subspace of $m$ qubits, we consider the problem 
$$ l = \min_V \text{tr}(V \rho) $$
$$ \text{s.t. } \bra{\psi^{\otimes m}} V \ket{\psi^{\otimes m}} \geq 0 \text{ for all } \ket{\psi} \in \mathbb{C}^2, \text{tr}(V) = m$$

Given a nonclassicality witness $V$, we detect the nonclassicality of $\rho$ if $l \lt 0$.
# 

In our paper, we discuss how to compute a converging sequence of upper bounds to $v_D$ and $l$ using **Reznick's Hierarchy** [[1]](#1) and explore the importance of going beyond sum-of-squares (SOS) certificates. Designed for the particular polynomials present in spin systems, we obtain an alternative sequence based on **Polya's theorem** [[2]](#2) and the **Fejér-Riesz theorem** [[3]](#3) which can detect nonclassicality in the case Reznick's Hierarchy is not practically applicable. Lower bounds are computed by relaxing nonnegativity on the complex plane to nonnegativity on a finite set of **rays**.  


# Dependencies
The code is written in the programming language Julia which can be downloaded here:
- [Julia](https://julialang.org/) 

We make use of the following Julia packages. To get started, open Julia in the command line via `julia`. 
Running Julia, install the packages via the commands:
```julia
julia> using Pkg
julia> Pkg.add("LinearAlgebra") 
julia> Pkg.add("Convex") 
julia> Pkg.add("JuMP")
julia> Pkg.add("PolyJuMP")
julia> Pkg.add("DynamicPolynomials")
julia> Pkg.add("SumOfSquares")
julia> Pkg.add("CSV")
julia> Pkg.add("DataFrames")

```
Moreover, we use [Mosek](https://www.mosek.com/) as a semidefinite programming (SDP) solver to check if a polynomial is a sum-of-squares (SOS). Note that Mosek requires a licence. 
We use the following packages:

```julia
julia> Pkg.add("Mosek")
julia> Pkg.add("MosekTools")
```
If you do not have a chance to obtain a licence for Mosek, you can instead for example install the [CSDP](https://docs.juliahub.com/CSDP/Gg3yk/1.1.1/) solver and replace every usage of Mosek. Note that this will require more individual work on the code itself which we do not further explain here.

# Getting started

You can start by creating some example states. We provide two librarys of example states: `example_states_light.jl` and`example_states_spins.jl`.

### Nonclassicality of light

For example, run the following command to create the third Fock state $\rho = \ket{3}\bra{3}$:

```julia
julia> include("PATH/TO/SCRIPT/example_states_light.jl")
julia> rho = fock_state(3)
```

You can test how its nonclassicality can be optimally detected by a SOS polynomial of degree $10$:

```julia
julia> include("PATH/TO/nonclassicality_light.jl")
julia> opt, witness = detect_fock_state_non_classicality_by_sos(rho, 10)
```


### Nonclassicality of spin-$\frac{1}{2}$ systems
For example, run the following command to create the generalized GHZ state $\rho$ with three qubits on the symmetric subspace:

```julia
julia> include("PATH/TO/example_states_spins.jl")
julia> rho = generalized_ghz_state(3)
```
You can check that $\rho$ is nonclassical via by running:
```julia
julia> include("PATH/TO/nonclassicality_spin_system.jl")
julia> call_method_nonclassicality_spin_system(rho, "SOS")
```


# Functionality
Here we list the important functions and their argument as well as optional arguments.

### Nonclassicality of light

In the file `nonclassicality_light.jl` we provide the following functions:

Detecting nonclassicality of cat states of the harmonic oscillator with SOS methods:
```julia
"""takes a cat state |a> + |-a> and computes the robustness to |a><a| + |-a><-a|"""
#=Input:
- a: Complex number to construct cat state |psi_a> = 1/N_{a} (|a> + |-a>)
- lvl: max degree of polynomial p corresponding to witness W_p
- optional  phi: Complex number:  |psi_a,phi> = 1/N (|a> + e^(i phi)|-a>)
- optional: sos_lvl: level in Reznick's hierarchy, by default sos_lvl=0: checks if p is SOS
- optional: specify solver
Output:
- objective value: 1/(1-min_W tr(W*R)) (robustness to |a><a| + |-a><-a|), with witness W, state R
- Witness W_p=#
function detect_cat_states_non_classicality_by_sos(a::Number, lvl::Int64; sos_lvl::Int64=0, phi::Number=0.0, solver=Mosek.Optimizer)
```
Detecting nonclassicality of state expanded in Fock basis with SOS methods:

```julia
"""takes a state rho = sum_ij rho_ij |i><j| and computes the robustness to int da <a|rho|a> |a><a|"""
#=Input:
- rho: Fock state rho = sum_ij rho_ij |i><j|
- lvl: max degree of polynomial p corresponding to witness W_p
- optional: sos_lvl: level in Reznick's hierarchy, by default sos_lvl=0: checks if p is SOS
- optional: specify solver
Output:
- objective value: 1/(1-min_W tr(W*R)) (robustness to |a><a| + |-a><-a|), with witness W, state R
- Witness W_p=#
function detect_fock_state_non_classicality_by_sos(rho::Matrix, lvl::Int64; sos_lvl::Int64=0, solver=Mosek.Optimizer)
```
Compute the moment matrix of a state expanded in the Fock basis:
```julia
"""Computes moment matrix of Fock states"""
#=Input:
- rho: state expanded in Fock basis
- lvl: to compute moment matrix degree 2D=lvl
Output:
- Moment matrix =#
```
Given a nonnegative polynomial (potentially not SOS), detect the most nonclassical state rho with maximal photon number cut_off:
```julia
"""Computes nonclassical state rho that minimises tr(W_P*rho) for a given nonnegative polynomial P"""
#=input:
- P: a nonnegative polynomial saved as a dictionary: map the exponents to its coefficient (as constructed in example_polynomials.jl)
- cut_off:  maximal number of photons in support of the state
- optional: semidefinite programming solver
- optional: pos_mom: additional constraint on the state to be not detectable by SOS polynomials of degree 
output:
- objective value: min_rho tr(W_P*rho),  witness W_P corresponding to polynomial P, state rho
- termination status of solver
- minimizer state rho=#
function maximal_non_classical_state_given_polynomial(P::Dict, cut_off::Int64; solver=Mosek.Optimizer, pos_mom=nothing)
```

### Nonclassicality of spin-$\frac{1}{2}$ systems

Given a symmetric state $\rho$ in the normalised Dicke basis, run the function `call_method_nonclassicality_spin_system(...)` in `nonclassicality_spin_system.jl` to test its nonclassicality using different methods.


```julia
"""call method for witnessing nonclassicality of spin state rho via nonnegative polynomials"""
#=input:
- state rho, in normalised Dicke basis
- method: 
        - String "SOS" for certifying the nonnegativity of polynomial p_V via sum of squares
        - String "Reznick", for using Reznick's hierarchy
        - String "Polya", for using our Polya-Fejer-Riesz hierarchy
        - String "rays", for a lower bound by via the finite rays method
- optional: 
        - deg_reznick: int, exponent in Reznick's hierarchy: p_V*(1+x^2+y^2)^deg_reznick
        - lvl_pfr: int, level of the Polya-Fejer_Riesz hierarchy
        - Nrays: int, number of finite rays in finite rays method, angles uniformly distributed
output: objective value tr(rho*V) of corresponding method, termination status of solver =#
```

# Data reproduction
Here we provide scripts that can directly reproduce the numerical results of our paper. 

### Nonclassicality of light
Running the following script guides you through the results we present in Figure 2 and Appendix D of the paper: There we construct nonclassical states that are not detectable by sum-of-squares (SOS) polynomials of degrees 6, 8 and 10 but by the Motzkin-Polynomial which is a nonnnegative polynomial of degree six that **is not SOS**. 

```julia
julia> include("PATH/TO/script_nonclassicality_light.jl")
```

### Nonclassicality of spin-$\frac{1}{2}$ systems
Running the following script guides you through the results we present in Figure 3 about a bound entangled $17$ qubit state derived the paper [[4]](#4). We apply Reznick's hierarchy, the Polya-Fejer-Riesz hierarchy and compute the lower bounds by the finite rays method on the example state of 17 qubits (Appendix G).

```julia
julia> include("PATH/TO/script_nonclassicality_spins.jl")
```
The results are automatically saved in the same directory as .csv files.


# References

<a id="1">[1]</a> B. Reznick, Uniform denominators in Hilbert's seventeenth problem, Mathematische Zeitschrift 220, 75 (1995).

<a id="1">[2]</a> G. Polya,  Über positive Darstellung von Polynomen, Vierteljschr. Natur-forsch. Ges. Zürich 73, 141–145 (1928).

 <a id="1">[3]</a> L. Fejér, Über trigonometrische Polynome, Journal für die reine und angewandte Mathematik 146, 53 (1916).

  <a id="1">[4]</a> J. Tura, A. Aloy, R. Quesada, M. Lewenstein, and
A. Sanpera, Separability of diagonal symmetric states:
a quadratic conic optimization problem, Quantum 2, 45
(2018).