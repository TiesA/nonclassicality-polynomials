using LinearAlgebra
using JuMP
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
using Convex

"""Example polynomials for detection of nonclassicality of light"""

## Examples of bivariate polynomials
# All polynomials are given as a dictionary, mapping each exponent to its coefficent

# Motzkin polynomial, Motzkin, 1967
function motzkin_poly_coeffs()
    R = Dict{Tuple, Number}()

    R[(4,2)] = 1.0
    R[(2,4)] = 1.0
    R[(2,2)] = -3.0
    R[(0,0)] = 1.0
    return R
end 

#Robinson polynomial, Robinson, 1973
function robinson_poly_coeffs()
    R = Dict{Tuple, Number}()
    R[(6,0)] = 1.0
    R[(0,6)] = 1.0
    R[(4,2)] = -1.0
    R[(2,4)] = -1.0
    R[(4,0)] = -1.0
    R[(0,4)] = -1.0
    R[(2,2)] = 3.0
    R[(2,0)] = -1.0
    R[(0,2)] = -1.0
    R[(0,0)] = 1.0

    return R
end 

#Choi-Lam polynomial, Choi and Lam, 1977
function choi_lam_poly_coeffs()
    R = Dict{Tuple, Number}()
    R[(4,2)] = 1.0
    R[(0,4)] = 1.0
    R[(2,0)] = 1.0
    R[(2,2)] = -3.0

    return R
end 

# polynomial corresponding to 4 qubit GHZ fidelity witness 
function ghz_4_like_polynomial_coeffs()
    R = Dict{Tuple, Number}()

    R[(6,0)] = 2.0
    R[(0,6)] = 2.0
    R[(4,2)] = 6.0
    R[(2,4)] = 6.0
    R[(4,0)] = 2.0
    R[(0,4)] = 2.0
    R[(2,2)] = 12.0
    R[(2,0)] = 2.0
    R[(0,2)] = 2.0

    return R 
end 

function w_4_like_polynomial_coeffs()
    R = Dict{Tuple, Number}()

    R[(8,0)] = 3/4
    R[(6,2)] = 3.0
    R[(4,4)] = 4.5
    R[(2,6)] = 3.0
    R[(0,8)] = 3/4

    R[(6,0)] = 3.0
    R[(0,6)] = 3.0
    R[(4,2)] = 9.0
    R[(2,4)] = 9.0

    R[(4,0)] = 4.5
    R[(0,4)] = 4.5
    R[(2,2)] = 9.0

    R[(2,0)] = -1.0
    R[(0,2)] = -1.0

    R[(0,0)] = 3/4

    return R
end 

#Nonnegative circuit polynomial
function sonc_test_polynomial_A()
    R = Dict{Tuple, Number}()

    R[(14, 16)] = 1.0
    R[(16, 14)] = 1.0

    R[(10, 10)] = -3.0

    R[(0, 0)] = 1.0

    return R 

end 

#Nonnegative circuit polynomial
function sonc_test_polynomial_B()
    R = Dict{Tuple, Number}()

    R[(8, 10)] = 1.0
    R[(10, 8)] = 1.0

    R[(6, 6)] = -3.0

    R[(0, 0)] = 1.0

    return R 

end 