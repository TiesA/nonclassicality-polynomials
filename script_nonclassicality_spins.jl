using LinearAlgebra
using Convex 
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
using CSV
using DataFrames

include("nonclassicality_spin_system.jl")
include("example_states_spins.jl")

print("\n\n")
print("Detect nonclassicality of 1/2-spin system:\n\n")
#sleep(2)
#print("In what follows, we consider the 17 qubit example state rho from Figure 3 and Appendix C.\n\n")
sleep(2)
print("In order to detect nonclassicality, we need tr(rho*V) < 0.\n\n")
sleep(2)
print("We explore different approaches to calculate the nonclassicality witness V by certifying that the corresponding polynomial p_V is nonnegative.\n\n")
sleep(2)
#Computes entangled/non-classical example state of 17 qubits, see Appendix C
rho = bound_entangled_family(17);

#Tries to witness the nonclassicality of rho based on p_V being a sum of squares (SOS)
#Input: state rho
#Output: tr(rho*V), (witness) matrix V, polynomial p_V
print("First, we constrain the polynomial p_V to be a sum of squares (SOS). This leads to a semidefinite program (SDP):\n\n")
#For this example, the matrix ist real, thus set real_mat = true to reduce variables
opt_value, status, mat = Nonclassicality_spin_system_by_SOS(rho)
print("The termination status of the solver is ", status, ".\n")
print("We receive as objective value tr(rho*V)=", opt_value, ".\n\n")
sleep(2)
print("We have not witnessed nonclassicality as we need tr(rho*V)<0.\n\n")
sleep(2)

#Tries to witness the nonclassicality of rho based on p_V(x,y)*(1+x^2+y^2)^N being SOS, for a constant integer (Reznick's hierarchy)
#Input state rho, "degree" of Reznick's hierachy deg
#Output: tr(rho*V), status of solver, (witness) matrix V
print("Next, we use Reznick's hierarchy to constrain p_V(x,y)*(1+x^2+y^2)^N to be SOS, for a given integer N.\n")
print("We demonstrate this for a few values of N.\n\n")
Max_N = 9
values_reznick = zeros(Float64, Max_N)
#Iteration through exponents of Reznick's hierarchy
for N = 1:Max_N
    local opt_value
    local status
    local mat
    opt_value, status, mat = Nonclassicality_spin_system_by_SOS(rho, deg=N, real_mat = true)
    print("With N=", N, " the termination status of the solver is ", status, ".\n")
    print("With N=", N, " we receive as objective value tr(rho*V)=", opt_value, ".\n\n")
    #to save to file
    values_reznick[N] = opt_value
end
#Write Data in csv.file 
degrees = [i for i in 1:Max_N]
df = DataFrame(N = degrees, opt_value = values_reznick)
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Reznick_hierarchy_spin_17_qubit_example.csv")
CSV.write(csv_file_path, df)

sleep(2)

print("You have probably noticed that the computation time increases for larger N due to the size of the underlying SDP.\n")
print("Still, we have not witnessed the nonclassicality of rho because we need tr(rho*V)<0 for a witness V.\n\n")
sleep(2)

#Tries to witness the nonclassicality of rho based on our Polya-Fejer-Riesz hierarchy, save solutions to .csv
#Input: state rho, level of hierachy
#Output: tr(rho*V), status of solver, (witness) matrix V
print("Next, we use our Polya-Fejer-Riesz-hierachy.\n")
print("We demonstrate this for a few levels of the hierarchy. \n\n")
max_lvl = 52
values_pfr = zeros(Float64, max_lvl)
#Iterate through levels of our Polya-Fejer-Riesz hierachy
for lvl = 1:max_lvl
    local opt_value
    local status
    local mat
    #For this example, the matrix ist real, thus set real_mat = true to reduce variables
    opt_value, status, mat = Nonclassicality_spin_system_by_polya_fejer_riesz_hierarchy(rho, lvl, real_mat = true)
    print("With level ", lvl, " the termination status of the solver is ", status, ".\n")
    print("With level ", lvl, " we receive as objective value tr(rho*V)=", opt_value, ".\n\n")
    #to save to file
    values_pfr[lvl] = opt_value
 end
#Write Data in csv.file 
levels = [i for i in 1:max_lvl]
df = DataFrame(level = levels, opt_value = values_pfr)
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Polya-Fejer-Riesz-hierarchy_spin_17_qubit_example.csv")
CSV.write(csv_file_path, df)

sleep(2)
print("We witness the nonclassicality of rho from level 12, since tr(rho*V)<0.\n\n")
sleep(2)

print("Last, we give a lower bound on tr(rho*V) by the finite ray approximation through univariate polynomials.\n")
print("We again demonstrate this on a few examples with different numbers of rays. \n\n")
min_rays = 40
max_rays = 400
#Array for optimal values
values_rays = []
#Array for number of rays
rays = []
#Iterate through number of rays
for Nrays in min_rays:20:max_rays
    local opt_value
    local status
    local mat
    #For this example, the matrix ist real, thus set real_mat = true to reduce variables
    opt_value, status = Nonclassicality_spin_system_by_finite_rays(rho, Nrays, real_mat = true)
    print("With ", Nrays, " rays the termination status of the solver is ", status, ".\n")
    print("With ", Nrays, " rays we receive as objective value tr(rho*V)=", opt_value, ".\n\n")
    #to save to file
    push!(values_rays, opt_value)
    push!(rays, Nrays)
end
#Write Data in csv.file 
df = DataFrame(rays = rays, opt_value = values_rays)
current_dir = @__DIR__
csv_file_path = joinpath(current_dir, "Finite_rays_spin_17_qubit_example.csv")
CSV.write(csv_file_path, df)

print("For this example state rho, the gap between the Polya-Fejer-Riesz hierarchy at level ", max_lvl, " and the lower bounds with ", max_rays, " rays is ", last(values_pfr)-last(values_rays), " .")
