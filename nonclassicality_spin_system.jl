using LinearAlgebra
using Convex 
using PolyJuMP
using Mosek
using MosekTools
using DynamicPolynomials
using SumOfSquares
include("example_states_spins.jl")

"""call method for witnessing nonclassicality of spin state rho via nonnegative polynomials"""
#=input:
- state rho, in normalised Dicke basis
- method: 
        - String "SOS" for certifying the nonnegativity of polynomial p_V via sum of squares
        - String "Reznick", for using Reznick's hierarchy
        - String "Polya", for using our Polya-Fejer-Riesz hierarchy
        - String "rays", for a lower bound by via the finite rays method
- optional: 
        - deg_reznick: int, exponent in Reznick's hierarchy: p_V*(1+x^2+y^2)^deg_reznick
        - lvl_pfr: int, level of the Polya-Fejer_Riesz hierarchy
        - Nrays: int, number of finite rays in finite rays method, angles uniformly distributed
output: objective value tr(rho*V) of corresponding method, termination status of solver =#

function call_method_nonclassicality_spin_system(rho::Matrix, method::String; deg_reznick::Int64 = 1, lvl_pfr::Int64 = 1, Nrays::Int64 = 50)
    if method == "SOS"
        opt, status, mat = Nonclassicality_spin_system_by_SOS(rho)
        print("Constrain polynomial p_V to be SOS: Objective value tr(rho*V)=", opt, " .\n")
    elseif method == "Reznick"
        opt, status, mat= Nonclassicality_spin_system_by_SOS(rho, deg = deg_reznick)
        print("Using Reznick's hierarchy and constrain p_V(x,y)*(1+x^2+y^2)^", deg_reznick, " to be SOS. Objective value tr(rho*V)=", opt, " .\n")
    elseif method == "Polya"
        opt, status, mat = Nonclassicality_spin_system_by_polya_fejer_riesz_hierarchy(rho, lvl_pfr)
        print("Using our Polya-Fejer-Riesz hierarchy with level ", lvl_pfr, ". Objective value tr(rho*V)=", opt, " .\n")
    elseif method == "rays"
        opt, status = Nonclassicality_spin_system_by_finite_rays(rho, Nrays)
        print("Computing a lower bound with ", Nrays, " rays. Objective value tr(rho*V)=", opt, ".\n")
    else 
        print("Your input was ", method, ". This is not a valid method. Please try again.\n")
        return nothing
    end
    return opt, status
end

"""checks whether a given symmetric state rho is nonclassical based on SOS witness V.
If the objective value tr(rho*V) <0 then rho is nonclassical."""
#=input: 
- state rho, in normalised Dicke basis
- deg: exponent Reznick's hierarchy: Check if (1+x^2+y^2)^deg*p_V is SOS
    - by default deg=0 tests if polynomial p_V is SOS
- silent: option to switch off output by solver
- real_mat: option to use only real valued matrix V 
output:
- objective value: (tr(rho*V))
- termination status of solver 
- matrix V, corresponding witness =#
function Nonclassicality_spin_system_by_SOS(rho::Matrix; deg::Int64=0, silent=true, real_mat=false)
    #dimension of state rho
    d = size(rho)[1]
    #real part of state rho
    RRho=real.(rho)
    #imaginary part of state rho
	IRho=imag.(rho)

    ###Initialise solver for semidefinite programming
    solver = optimizer_with_attributes(Mosek.Optimizer, MOI.Silent() => silent)
	model= Model(solver)

    ###Initialise Hermitian matrix V with decision variables
    #real part of V 
    RV = @variable(model,[1:d,1:d],Symmetric)
    #imaginary part of V
    if !real_mat
        IV = @variable(model, Im[1:d,1:d] in SkewSymmetricMatrixSpace())
    end 
    #Renormalise matrix V
    renormalisation_matrix = zeros(Float64, d, d)
    for i in 1:d
        for j in 1:d
            renormalisation_matrix[i,j] = sqrt(binomial(d-1,i-1)*binomial(d-1,j-1))
        end 
    end 
    RV_renorm = RV ./ renormalisation_matrix
    if !real_mat
        IV_renorm = IV ./ renormalisation_matrix
    end

    ###Initialise polynomial p_V = adjoint(u)*V*u
    @polyvar x y
    u = [(x + im * y)^i for i in 0:d-1]
    if !real_mat
        p = adjoint(u)*(RV + im * IV)*u
    else #real valued matrix V
        p = adjoint(u)*RV*u
    end 
    
    #By default, deg = 0, then p = Polynomial(real(p.a), p.x)
    #Else compute polynomial p based on Reznick's hierarchy
    p = (1+x^2+y^2)^deg * Polynomial(real(p.a), p.x)

    ###Initialise SDP

    #Compute objective function obj = tr(V*rho)
    if !real_mat
        obj=tr(RV_renorm*RRho - IV_renorm*IRho)
    else #real valued matrix V
        obj=tr(RV_renorm*RRho)
    end
	@objective(model,Min,obj)

    #constraint to achieve dual feasibility
    @constraint(model, tr(RV_renorm) == Float64(d))

    #contrain the polynomial p to be a sum of squares (SOS)
    @constraint(model, cp, p in SOSCone())

    ###Solve resulting SDP
    optimize!(model)

    #@show termination_status(model)

    ###Output witness matrix V
    if !real_mat
        mat = value.(RV_renorm) + im * value.(IV_renorm)
    else 
        mat = value.(RV_renorm)
    end 

    return objective_value(model), termination_status(model), mat
end 


"""uses finite ray approximation of the complex plane to obtain lower bound on tr(rho*V) 
for a state rho and a witness V, uses equally distributed angles
"""
#=input:
- state rho, in normalised Dicke basis
- Nrays: the number of lines which correspond to equally distributed angles between 0 and Pi
- real_mat: real_mat: option to use only real valued matrix V
output:
- objective value: (tr(rho*V)) 
- termination status of solver =#
function Nonclassicality_spin_system_by_finite_rays(rho::Matrix, Nrays::Int64; real_mat=false)
    #dimension of state rho
    d = size(rho)[1]
    #real part of state rho
    RRho=real.(rho)
    #imaginary part of state rho
	IRho=imag.(rho)

    ###Initialise solver for semidefinite programming
    solver = optimizer_with_attributes(Mosek.Optimizer, MOI.Silent() => true)
	model= Model(solver)

    ###Initialise Hermitian matrix V with decision variables
    #real part of V
    RV = @variable(model,[1:d,1:d],Symmetric)
    #imaginary part of V
    if !real_mat
        IV = @variable(model, Im[1:d,1:d] in SkewSymmetricMatrixSpace())
    end 
    #renormalise matrix V
    renormalisation_matrix = zeros(Float64, d, d)
    for i in 1:d
        for j in 1:d
            renormalisation_matrix[i,j] = sqrt(binomial(d-1,i-1)*binomial(d-1,j-1))
        end 
    end 
    RV_renorm = RV ./ renormalisation_matrix 
    if !real_mat
        IV_renorm = IV ./ renormalisation_matrix
    end   

    ###Initialise Nrays many univariate polynomials in polar form by equally distributed angles
    @polyvar x
    #Monomial vector
    u = [x^i for i in 0:d-1]
    #Nrays many angles
    angles = range(0, pi, Nrays+1)[1:Nrays]
    #coefficients of each polynomial stored as diagonal matrices
    U = [Diagonal([exp(im*i*j) for j in 0:d-1]) for i in angles] 

  
    ###Initialise SDP

    #Compute objective function obj = tr(V*rho)
    if !real_mat
        obj=tr(RV_renorm*RRho - IV_renorm*IRho)
    else
        obj=tr(RV_renorm*RRho)
    end
	@objective(model,Min,obj)

    #constraint to achieve dual feasibility
    @constraint(model, tr(RV_renorm) == d)

    #construct each univariate polynomail and constrain to be SOS
    for i in 1:Nrays
        if !real_mat
            p = adjoint(u)*adjoint(U[i])*(RV + im * IV)*U[i]*u
        else 
            p = adjoint(u)*adjoint(U[i])*RV*U[i]*u
        end 
        p = Polynomial(real(p.a), p.x)
        @constraint(model, p in SOSCone())
    end 

    ###Solve resulting SDP
    optimize!(model)

    #@show termination_status(model)

    return objective_value(model), termination_status(model)
end 


"""uses finite ray approximation of the complex plane to obtain lower bound on tr(rho*V) 
for a state rho and a witness V, uses given vector of angles between 0 and pi
"""
#=input:
- state rho, in normalised Dicke basis
- rays: array of angles between 0 and Pi
- real_mat: real_mat: option to use only real valued matrix V
output:
- objective value: (tr(rho*V)) 
- termination status of solver =#
function Nonclassicality_spin_system_by_finite_rays(rho::Matrix, rays::Array; real_mat=false)
    #dimension of state rho
    d = size(rho)[1]
    #real part of state rho
    RRho=real.(rho)
    #imaginary part of state rho
	IRho=imag.(rho)

    ###Initialise solver for semidefinite programming
    solver = optimizer_with_attributes(Mosek.Optimizer, MOI.Silent() => true)
    model= Model(solver)

    ###Initialise Hermitian matrix V with decision variables
    #real part of V 
    RV = @variable(model,[1:d,1:d],Symmetric)
    #imaginary part of V
    if !real_mat
        IV = @variable(model, Im[1:d,1:d] in SkewSymmetricMatrixSpace())
    end 
    #Renormalise matrix V
    renormalisation_matrix = zeros(Float64, d, d)
    for i in 1:d
        for j in 1:d
            renormalisation_matrix[i,j] = sqrt(binomial(d-1,i-1)*binomial(d-1,j-1))
        end 
    end 
    RV_renorm = RV ./ renormalisation_matrix
    if !real_mat
        IV_renorm = IV ./ renormalisation_matrix
    end

    ###Initialise univariate polynomials in polar form with each angle given in vector rays
    @polyvar x
    #Monomial vector
    u = [x^i for i in 0:d-1]
    #coefficients of each polynomial stored as diagonal matrices
    U = [Diagonal([exp(im*i*j) for j in 0:d-1]) for i in rays]


    ###Initialise SDP

    #Compute objective function obj = tr(rho*V)
    if !real_mat
        obj=tr(RV_renorm*RRho - IV_renorm*IRho)
    else
        obj=tr(RV_renorm*RRho)
    end
	@objective(model,Min,obj)

    #constraint to achieve dual feasibility
    @constraint(model, tr(RV_renorm) == d)

     #construct each univariate polynomail and constrain to be SOS
    for i in 1:length(rays)
        if !real_mat
            p = adjoint(u)*adjoint(U[i])*(RV + im * IV)*U[i]*u
        else 
            p = adjoint(u)*adjoint(U[i])*RV*U[i]*u
        end 
        p = Polynomial(real(p.a), p.x)
        @constraint(model, p in SOSCone())
    end 

    ###Solve resulting SDP
    optimize!(model)

    #@show termination_status(model)

    return objective_value(model), termination_status(model)
end 


"""Uses our Polya-Fejer-Riesz hierarchy on witness V to check whether given symmetric state is nonclassical 
If the objective value tr(rho*V) <0 then rho is nonclassical."""
#=input:
- state rho, in normalised Dicke basis
- lvl: level of Polya-Fejer-Riesz hierarchy, we consider the polynomial (1+r)^{lvl}*p_V(r*e^{-i*theta}, r*e^{i*theta})
- real_mat: option to use only real valued matrix V
- prefactor: option to use (1+r^prefactor) instead of (1+r)
- solver: option to use different solver
output:
- objective value: (tr(rho*V))
- termination status of solver
- matrix V, corresponding witness =#
function Nonclassicality_spin_system_by_polya_fejer_riesz_hierarchy(rho::Matrix, lvl::Int64; real_mat::Bool=false, prefactor::Int64=1,solver=Mosek.Optimizer)
    d = size(rho)[1]
    lvl += 1
    #real part of rho
    RRho=real.(rho)
    if !real_mat
        #imaginary part of rho
	    IRho=imag.(rho)
    end 

    ###Initialise semidefinite programming solver
    solver = optimizer_with_attributes(solver, MOI.Silent() => true)
	model= Model(solver)

    ###Initialise witness matrix V
    
    #Real part of V
    RV = @variable(model, Re[1:d,1:d], Symmetric)
    #Imaginary part of V
    if !real_mat
        IV = @variable(model, Im[1:d,1:d] in SkewSymmetricMatrixSpace())
    end 
    #Renomalise of V
    renormalisation_matrix = zeros(Float64, d, d)
    for i in 1:d
        for j in 1:d
            renormalisation_matrix[i,j] = sqrt(binomial(d-1,i-1))*sqrt(binomial(d-1,j-1))
        end 
    end 
    RV_renorm = RV ./ renormalisation_matrix
    if !real_mat
        IV_renorm = IV ./ renormalisation_matrix
    end 

    ### Initialise trig polynomials

    #construct initial trig coefficients
    init_coeff_re = zeros(AffExpr, 2*d-1, d)
    if !real_mat
        init_coeff_imag = zeros(AffExpr, 2*d-1, d)
    end 

    for i in 1:d
        j = 1 
        l = 2*i - 1
        k = i
        m = 1
        while j <= d && k<=d
            init_coeff_re[l, m] = RV[i, k] 
            if !real_mat
                init_coeff_imag[l, m] = IV[i, k]
            end 
            l += 1
            k += 1
            j += 1
            m += 1
        end 
    end 

    if !real_mat
        init_coeff = init_coeff_re + im * init_coeff_imag
    else 
        init_coeff = init_coeff_re
    end 

    #Compute all trig coefficients recursively from initial coeffs by calling helper functions
    init_coeff = [init_coeff[i,:] for i in 1:2*d - 1]

    coeff = generalized_get_trig_poly_coeffs(init_coeff, lvl, prefactor)

    L = length(coeff)

    #scale coefficients for better stability
    coeff = [(1/(lvl)^4)*coeff[i] for i in 1:L]

    matrix_size = repeat([d], L) 

    for i in 1:L
        j = d 
        while true
            if coeff[i][j] != 0
                break
            end 
            pop!(coeff[i])
            matrix_size[i] -= 1
            j -= 1
        end 
    end

    if !real_mat
        pos = [@variable(model, [1:matrix_size[i],1:matrix_size[i]] in HermitianPSDCone()) for i in 1:L]
    else 
        pos = [@variable(model, [1:matrix_size[i],1:matrix_size[i]], PSD) for i in 1:L]
    end 

    ###Initalise SDP

    ## Constraints for non-negativity of trig polynomials
    for i in 1:L
        s = matrix_size[i]
        T = unit_toeplitz(s)
        for j in s:length(T)
            @constraint(model, tr(adjoint(T[j])*pos[i]) == coeff[i][j - s + 1])
        end 

    end 

    #Objective function
    if !real_mat
        obj=tr(RV_renorm*RRho - IV_renorm*IRho)
    else 
        obj=tr(RV_renorm*RRho)
    end 
	@objective(model, Min, obj)

    #constraint to achieve dual feasibility
    @constraint(model, tr(RV_renorm) == d)

    ###Solve corresponding SDP
    optimize!(model)

    #return witness V
    if !real_mat
        mat = value.(RV)+value.(IV)
    else 
        mat = value.(RV)
    end 

    #@show termination_status(model)
    return objective_value(model), termination_status(model), mat  
end 


"""Helper functions for Nonclassicality_spin_system_by_polya_fejer_riesz hierachy"""

#For recursive structure of trig coefficients: construct coefficents for next level from current level
function add_trig_coeffs_by_polya(init_coeffs::Vector)
    N = length(init_coeffs)
    coeffs = Vector(undef, N + 1)
    coeffs[1] = init_coeffs[1]
    coeffs[N + 1] = init_coeffs[N]
    
    for i in 2:N
        coeffs[i] = init_coeffs[i - 1] + init_coeffs[i]
    end 
    return coeffs
end 

#For recursive structure of trig coefficients: construct coefficents for next level from current level
#Generalisation for prefactors (1+r^l)
function generalized_add_trig_coeffs_by_polya(init_coeffs::Vector, l::Int64)
    N = length(init_coeffs)
    coeffs = Vector(undef, N + l)
    for i in 1:l
        coeffs[i] = init_coeffs[i]
        coeffs[N+i] = init_coeffs[N-l+i]
    end 
    
    for i in l+1:N
        coeffs[i] = init_coeffs[i - l] + init_coeffs[i]
    end 
    return coeffs
end 

#Recursively call add_trig_coeffs_by_polya to construct coeffs of given level from initial coeffs
function get_trig_poly_coeffs(init_coeffs::Array, lvl::Int64)
    i = 1
    coeffs = init_coeffs
    while i < lvl
        coeffs = add_trig_coeffs_by_polya(coeffs)
        i += 1 
    end 
    return coeffs
end 

#Recursively call add_trig_coeffs_by_polya to construct coeffs of given level from initial coeffs
#Generealisation of prefactors (1+r^l)
function generalized_get_trig_poly_coeffs(init_coeffs::Array, lvl::Int64, l::Int64)
    i = 1
    coeffs = init_coeffs
    while i < lvl
        coeffs = generalized_add_trig_coeffs_by_polya(coeffs, l)
        i += 1 
    end 
    return coeffs
end 

#Create the list of unit Toeplitz matrices
function unit_toeplitz(d::Int64)
    N = 2 * (d-1) + 1
    list = [zeros(ComplexF64, d, d) for i in 1:N]
    for i in -(d-1):d-1
        for m in 1:d
            for n in 1:d
                if n - m == i 
                    list[i+d][m, n] = 1.0
                end 
            end 
        end 
    end
    return list
end 



"""Check nonclassicality of Dicke diagonal state"""
#=input: Dicke diagonal state rho
output: objective value tr(rho*V), witness V=#
function dicke_diagonal_nonclassicality_by_sos(rho::Matrix)
    #dimension of state rho
    d = size(rho)[1]

    ###Initialise solver for semidefinite programming
    solver = optimizer_with_attributes(Mosek.Optimizer, MOI.Silent() => true)
	model= Model(solver)

    ###Initialise real matrix V with decision variables
    RV = @variable(model, [1:d])

    ###Initialise polynomial p_V
    @polyvar r 

    p = sum([RV[i]*r^(2*(i-1)) for i in 1:d])

    ###Initialise SDP

    #objective function
    obj = sum([(1/binomial(d-1,i-1))*RV[i]*real(rho[i, i]) for i in 1:d])
    @objective(model,Min,obj)

    #constraints
    trace = sum([(1/binomial(d-1,i-1))*RV[i] for i in 1:d])
    @constraint(model, trace == d)

    #constrain p_V to be SOS
    @constraint(model, p in SOSCone())

    ###Solve SDP
    optimize!(model)

    @show termination_status(model)

    return objective_value(model), value.(RV)
end




